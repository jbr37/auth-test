'use strict'

const TokenService = require('./services/token.service');
const moment = require('moment');

//Datos para simulacion

const miPass = "miContraseña";
const badPass = "miOtraContraseña"
const usuario = {
    _id: '123456789',
    email: 'jbr37@gcloud.ua.es',
    displayName: 'JAvier Botella',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//Creamos un token
const token = TokenService.creaToken(usuario);
//console.log(token);

//Decodificar el token

TokenService.decodificaToken(token)
    .then( userId => {
        return console.log(`ID1: ${userId}`)
    })
    .catch( err=> console.log(err));

// Decodificar token erroneo +
const badToken = 'eyJhbGddciOiJIUzI1iIsInR5cCI6IkpXVCJ9s.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c';
TokenService.decodificaToken(badToken)
    .then( userId => {
        return console.log(`ID2: ${userId}`)
    })
    .catch( err=> console.log(err));