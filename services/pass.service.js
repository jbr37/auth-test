'use strict'

const bcrypt = require('bcrypt');

// encriptaPassword
// Devuelve un hash con un salt incluido en formato 

// $2b$10$O6.yYo0.2udN5t00CjcqPejbBAWCS6iTZ3Vlfr4gPK9rixjhbGwq.
// *******----------------------*******************************
// ALg Coste        Salt            Hash

function encriptaPassword( password ){
    return bcrypt.hash( password, 10);
}

// comparaPassword
//
// Devolver verdadero o falso si coincide o no el password y el hash
//
function comparaPassword( password, hash){
    return bcrypt.compare(password, hash);
}

module.exports = {
    encriptaPassword,
    comparaPassword
};

