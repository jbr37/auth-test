'use strict'

const PassService = require('./services/pass.service');
const moment = require('moment');

//Datos para simulacion

const miPass = "miContraseña";
const badPass = "miOtraContraseña"
const usuario = {
    _id: '123456789',
    email: 'jbr37@gcloud.ua.es',
    displayName: 'JAvier Botella',
    password: miPass,
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};

console.log(usuario);

//EncriEncriptamos el password

PassService.encriptaPassword(usuario.password)
.then( hash => {
    usuario.password = hash;
    console.log(usuario);


    //Verificamos password
    PassService.comparaPassword(miPass, usuario.password)
    .then (isOk => {
        if (isOk){
            console.log('p1: pass correcto');
        }
        else{
            console.log('p1: el pass no es correcto');
        }
    })
    .catch(err => console.log(err));

    //Verificamos password contra un pass falso
    PassService.comparaPassword(badPass, usuario.password)
    .then (isOk => {
        if (isOk){
            console.log('p2: pass correcto');
        }
        else{
            console.log('p2: el pass no es correcto');
        }
    })
    .catch(err => console.log(err));


})