'use strict'

const bcrypt = require('bcrypt');

//formato del hash 
// $2b$10$O6.yYo0.2udN5t00CjcqPejbBAWCS6iTZ3Vlfr4gPK9rixjhbGwq.
// *******----------------------*******************************
// ALg Coste        Salt            Hash
//Datos para simulacion

const miPass = "miContraseña";
const badPass = "miOtraContraseña"


// salt = bcrypt.salt( 10 );
// hash = bcrypt.has( miPass, salt);
// db.users.update(id, hash);
// db.account.hash.update(id, salt);

// Creamos el Salt
bcrypt.genSalt(10, (err, salt) => {
    console.log(`Salt 1: ${salt}`);

    //Utilizamos el salt para generar un hash
    bcrypt.hash( miPass, salt, (err, hash) => {
        if (err) console.log(err);
        else console.log(`Hash 1: ${hash}`)
    })
})


// Creamos el hash directamente
bcrypt.hash(miPass, 10, (err, hash) => {
    if (err) console.log(err);
    else {
        console.log(`Hash 2: ${hash}`);

        // Comprobamos utilizando la contraseña correcta...
        bcrypt.compare( miPass, hash, (err, result)=>{
            console.log(`Result2.1: ${result}`);
        })
        //Comprobamos utilizandfo la contraseña incorrecta
        bcrypt.compare( miPass, badPass, (err, result)=>{
            console.log(`Result2.2: ${result}`);
        })
    }
})